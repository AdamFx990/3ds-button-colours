#include <string.h>
#include <stdio.h>

#include <3ds.h>

int main(int argc, char **argv) {

	gfxInitDefault();
	consoleInit(GFX_TOP, NULL);

	printf("Hello 3DS World!");
int i = 0;
	// Main loop
	while (aptMainLoop()) {
		hidScanInput();
		u32 kDown = hidKeysDown();

		gspWaitForVBlank();
		

		// Your code goes here
		if (kDown & KEY_SELECT){
			printf("\x1b[4%im", i++);
				if(i >= 7)
					i = 0;
		}
		if (kDown & KEY_START)
			break; // break in order to return to hbmenu
		if(kDown & KEY_A) 
			printf("\x1b[31m");
		if(kDown & KEY_B)
			printf("\x1b[33m");
		if(kDown & KEY_Y)
			printf("\x1b[32m");
		if(kDown & KEY_X)
			printf("\x1b[34m");
		if (kDown) {
			printf("Colours\n\x1b");
		}
		// Flush and swap framebuffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	gfxExit();
	return 0;
}
